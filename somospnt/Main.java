package somospnt;

public class Main
{
    public static void main(String[] args)
    {
        Supermercado test = new Supermercado();
        
        test.cargarListaDeProductos();
		
        test.mostrarProductos();

        test.productoMasCaro();
        test.productoMasBarato(); 
    }
    
}
