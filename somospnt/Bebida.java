package somospnt;

public class Bebida extends Producto
{
    public Bebida(String nombre, double capacidad, String tipoUnidad, double precio)
    {
        super(nombre, capacidad, tipoUnidad, precio);
    }
    
    public void mostrarDetalle()
    {
        System.out.println("Nombre: "+this.getNombre()+" /// "+this.getTipoUnidad()+": "+this.getCapacidad()+" /// " +"Precio: "+"$"+(int)this.getPrecio());
    }
}
