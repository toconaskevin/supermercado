package somospnt;

import java.util.LinkedList;
import java.util.List;

public class Supermercado
{
    private final List<Producto> listaDeProductos;
    
    public Supermercado()
    {
        this.listaDeProductos=new LinkedList<>();
    }
	
    public void agregarProducto(Producto p)
    {
        this.listaDeProductos.add(p);
    }
		
    public void cargarListaDeProductos()
    {
        Producto producto1=new Bebida("Coca-Cola Zero", 1.5, "Litros", 20);
        Producto producto2=new Bebida("Coca-Cola", 1.5, "Litros", 18);
        Producto producto3=new Shampoo("Shampoo Sedal", 500, "Contenido", 19);
        Producto producto4=new Fruta("Frutillas", 1, "kilo", 64);
		
        this.agregarProducto(producto1);
        this.agregarProducto(producto2);
        this.agregarProducto(producto3);
        this.agregarProducto(producto4);
    }
    
    public void mostrarProductos()
    {
        listaDeProductos.forEach(Producto::mostrarDetalle);
        System.out.println("==============================");
    }
    
    public void productoMasCaro()
    {
        Producto masCaro;
        masCaro=this.listaDeProductos.get(0);
        
        for (Producto producto : listaDeProductos)
        {	
            if (producto.compareTo(masCaro)>0)
            {
                masCaro=producto;
            }
        }
		
        System.out.println("Producto más caro: "+masCaro.getNombre()); 
    }
	
    public void productoMasBarato() {
        Producto masBarato;
        masBarato=this.listaDeProductos.get(0);
		
        for (Producto producto : listaDeProductos)
        {
            if (producto.compareTo(masBarato)<0)
            {
                masBarato=producto;
            }
        }
        
        System.out.println("Producto más barato: "+masBarato.getNombre());
    }
}
