package somospnt;

public abstract class Producto implements Comparable<Producto>
{
    private String nombre;
    private double capacidad;
    private String tipoUnidad;
    private double precio;
    
    public Producto()
    {
        this.nombre=null;
        this.capacidad=0;
        this.tipoUnidad=null;
        this.precio=0;
    }
    
    public Producto(String nombre, double capacidad, String tipoUnidad, double precio)
    {
        this.nombre=nombre;
        this.capacidad=capacidad;
        this.tipoUnidad=tipoUnidad;
        this.precio=precio;
    }

    public String getNombre()
    {
        return nombre;
    }
    
    public double getCapacidad()
    {
        return capacidad;
    }

    public String getTipoUnidad()
    {
        return tipoUnidad;
    }

    public double getPrecio()
    {
        return precio;
    }

    @Override
    public int compareTo(Producto o)
    {
        int compare;
        if(this.getPrecio()>o.getPrecio()){
            compare = 1;
        }
        else{
            if(this.getPrecio()<o.getPrecio()){
                compare= -1;
            }
            else{
                compare = 0;
            }
        }
        return compare;
    }
    
    public abstract void mostrarDetalle();
}
