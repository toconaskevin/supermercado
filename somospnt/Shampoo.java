package somospnt;

public class Shampoo extends Producto
{    
    public Shampoo(String nombre, double capacidad, String tipoUnidad, double precio)
    {
        super(nombre, capacidad, tipoUnidad, precio);
    }
    
    public void mostrarDetalle()
    {
        System.out.println("Nombre: "+this.getNombre()+" /// "+this.getTipoUnidad()+": "+(int)this.getCapacidad()+"ml /// " +"Precio: "+"$"+(int)this.getPrecio());
    }
}
