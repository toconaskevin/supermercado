package somospnt;

public class Fruta extends Producto
{
    public Fruta(String nombre, double capacidad, String tipoUnidad, double precio)
    {
        super(nombre, capacidad, tipoUnidad, precio);
    }
    
    public void mostrarDetalle()
    {
        System.out.println("Nombre: "+this.getNombre()+" /// "+"Precio: "+"$"+(int)this.getPrecio()+" /// "+"Unidad de venta: "+this.getTipoUnidad());
    }
}
